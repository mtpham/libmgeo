# libmgeo
Minh-Tri's C++ template library for geometry

Based on Minh-Tri's paper:

[1] Pham et al, Distances and Means of Direct Similarities, IJCV, 2014.
    http://mi.eng.cam.ac.uk/~cipolla/publications/article/2014-IJCV-Pham.pdf

